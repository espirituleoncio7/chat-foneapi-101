import {
  Box,
  Container,
  Text,
  Tabs,
  TabList,
  TabPanels,
  TabPanel,
  Tab,
} from '@chakra-ui/react';

import React, { useEffect } from 'react';
import Login from '../components/authentication/Login';
import SignUp from '../components/authentication/signup';
import { useNavigate } from 'react-router-dom';

const styleBox1 = {
  d: 'flex',
  justifyContent: 'center',
  p: 4,
  bg: 'white',
  w: '100%',
  m: '40px 0 5px 0',
  borderRadius: 'lg',
};

const styleBox2 = {
  d: 'flex',
  justifyContent: 'center',
  p: 4,
  bg: 'white',
  w: '100%',
  m: '0 0 15px 0',
  borderRadius: 'lg',
};

const Homepage = () => {
  const navigate = useNavigate();

  useEffect(() => {
    const userInfo = JSON.parse(localStorage.getItem('userInfo'));

    if (userInfo) {
      navigate('/chats');
    }
  }, [navigate]);

  return (
    <Container maxW="xl" centerContent>
      <Box {...styleBox1}>
        <center>
          <Text fontSize={'3xl'} color={'silver'}>
            FoneAPI Chat System
          </Text>
        </center>
      </Box>

      <Box {...styleBox2}>
        <Tabs variant="soft-rounded" colorScheme="teal">
          <TabList mb={'2'}>
            <Tab width={'50%'}>Login</Tab>
            <Tab width={'50%'}>Sign Up</Tab>
          </TabList>
          <TabPanels>
            <TabPanel>
              <Login />
            </TabPanel>
            <TabPanel>
              <SignUp />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </Box>
    </Container>
  );
};

export default Homepage;
