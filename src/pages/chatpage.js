import React, { useState } from 'react';
import { useChatContext } from '../contextAPI/chatProvider';
import SideDrawer from '../components/miscellaneous/sideDrawer';

import MyChats from '../components/MyChats';
import ChatBox from '../components/ChatBox';

import { Box } from '@chakra-ui/react';

const Chatpage = () => {
  const [fetchAgain, setFetchAgain] = useState(false);
  const { user } = useChatContext();

  return (
    <Box w={'100%'}>
      {user && <SideDrawer />}
      <Box display="flex" justifyContent="space-between" w="100%" h="91.5vh" p="10px">
        {user && <MyChats fetchAgain={fetchAgain} />}
        {user && <ChatBox fetchAgain={fetchAgain} setFetchAgain={setFetchAgain} />}
      </Box>
    </Box>
  );
};

export default Chatpage;
