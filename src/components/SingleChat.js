import React, { useEffect, useRef, useState } from 'react';
import { useChatContext } from '../contextAPI/chatProvider';
import { Box, FormControl, IconButton, Input, Spinner, Text, useToast } from '@chakra-ui/react';
import { ArrowBackIcon } from '@chakra-ui/icons';
import { getSender, getSenderFull } from './config/ChatLogics';
import ProfileModal from './miscellaneous/Profile';
import axios from 'axios';
import { io } from 'socket.io-client';

import './style.css';
import ScrollableChat from './ScrollableChat';
import Loader from './Loader';

const SingleChat = ({ fetchAgain, setFetchAgain }) => {
  const ENDPOINT = 'http://localhost:5000/'; // "https://chat-foneapi-leon.onrender.com"; -> After deployment

  const [selectedChatCompare, setSelectedChatCompare] = useState();

  const socket = useRef();
  const [socketConnected, setSocketConnected] = useState(false);
  const [typing, setTyping] = useState(false);
  const [istyping, setIsTyping] = useState(false);

  const { user, selectedChat, setSelectedChat, notification, setNotification } = useChatContext();
  const [messages, setMessages] = useState([]);
  const [loading, setLoading] = useState(false);
  const [newMessage, setNewMessage] = useState('');
  const toast = useToast();

  useEffect(() => {
    socket.current = io(ENDPOINT);
    socket.current.emit('setup', user);
    socket.current.on('connected', () => setSocketConnected(true));
    socket.current.on('typing', () => setIsTyping(true));
    socket.current.on('stop typing', () => setIsTyping(false));

    // eslint-disable-next-line
  }, []);

  const fetchMessages = async () => {
    if (!selectedChat) return;

    try {
      const config = {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      };

      setLoading(true);

      const { data } = await axios.get(`/api/message/${selectedChat._id}`, config);

      //   console.log(messages);
      setMessages(data);
      setLoading(false);

      socket.current.emit('join chat', selectedChat._id);
    } catch (error) {
      //   toast({
      //     title: 'Error Occured!',
      //     description: 'Failed to Load the Messages',
      //     status: 'error',
      //     duration: 1500,
      //     isClosable: true,
      //     position: 'bottom',
      //   });
      //   console.log(error);
    }
  };

  const sendMessage = async (event) => {
    if (event.key === 'Enter' && newMessage) {
      socket.current.emit('stop typing', selectedChat._id);
      try {
        const config = {
          headers: {
            'Content-type': 'application/json',
            Authorization: `Bearer ${user.token}`,
          },
        };
        setNewMessage('');
        const { data } = await axios.post(
          '/api/message',
          {
            content: newMessage,
            chatId: selectedChat,
          },
          config
        );

        // socket
        socket.current.emit('new message', data);

        setMessages([...messages, data]);
      } catch (error) {
        toast({
          title: 'Error Occured!',
          description: 'Failed to send the Message',
          status: 'error',
          duration: 5000,
          isClosable: true,
          position: 'bottom',
        });
      }
    }
  };

  useEffect(() => {
    fetchMessages();

    // edited this code into state

    setSelectedChatCompare((previous) => (previous = selectedChat));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedChat]);

  useEffect(() => {
    socket.current.on('message recieved', (newMessageRecieved) => {
      if (
        !selectedChatCompare || // if chat is not selected or doesn't match current chat
        selectedChatCompare._id !== newMessageRecieved.chat._id
      ) {
        if (!notification.includes(newMessageRecieved)) {
          setNotification([newMessageRecieved, ...notification]);
          setFetchAgain(!fetchAgain);
        }
      } else {
        setMessages([...messages, newMessageRecieved]);
      }
    });
  });

  const typingHandler = (e) => {
    setNewMessage(e.target.value);

    if (!socketConnected) return;

    if (!typing) {
      setTyping(true);
      socket.current.emit('typing', selectedChat._id);
    }
    let lastTypingTime = new Date().getTime();
    var timerLength = 3000;
    setTimeout(() => {
      var timeNow = new Date().getTime();
      var timeDiff = timeNow - lastTypingTime;
      if (timeDiff >= timerLength && typing) {
        socket.current.emit('stop typing', selectedChat._id);
        setTyping(false);
      }
    }, timerLength);
  };

  return (
    <>
      {selectedChat ? (
        <>
          <Box display={'flex'} justifyContent={'space-between'} width={'100%'}>
            <IconButton
              colorScheme="teal"
              d={{ base: 'flex', md: 'none' }}
              icon={<ArrowBackIcon />}
              onClick={() => setSelectedChat('')}
            />

            {messages && (
              <>
                <Text textAlign={'center'} fontSize={'25px'} fontWeight={'bold'} color={'teal'} fontStyle={'italic'}>
                  {getSender(user, selectedChat.users)}
                </Text>
                <ProfileModal user={getSenderFull(user, selectedChat.users)} />
              </>
            )}
          </Box>

          <Box
            display="flex"
            flexDir="column"
            justifyContent="flex-end"
            p={3}
            bg="#f0f0f0"
            w="100%"
            h="100%"
            borderRadius="lg"
            overflowY="hidden"
            mt={5}
          >
            {loading ? (
              <Spinner color="teal" size="xl" w={20} h={20} alignSelf="center" margin="auto" />
            ) : (
              <div className="messages">
                <ScrollableChat messages={messages} />
              </div>
            )}

            <FormControl onKeyDown={sendMessage} id="first-name" isRequired mt={3}>
              {istyping ? (
                <div>
                  <Loader />
                </div>
              ) : (
                <></>
              )}
              <Input
                variant="filled"
                bg="#E0E0E0"
                placeholder="Enter a message.."
                value={newMessage}
                onChange={typingHandler}
                h={'70px'}
              />
            </FormControl>
          </Box>
        </>
      ) : (
        <Box display={'flex'} alignItems={'center'} justifyContent={'center'} h={'100%'}>
          <Text fontSize={'3xl'} pb={'3'} color={'silver'} fontStyle={'italic'} userSelect={'none'}>
            Click on Contact to Start Chat
          </Text>
        </Box>
      )}
    </>
  );
};

export default SingleChat;
